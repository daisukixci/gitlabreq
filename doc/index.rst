.. gitlabreq documentation master file, created by
   sphinx-quickstart on Sun Nov  8 22:48:00 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to gitlabreq's documentation!
=====================================

.. automodule:: gitlabreq
    :members:
.. automodule:: gitlabreq.extends
    :members:
.. automodule:: gitlabreq.file
    :members:
.. automodule:: gitlabreq.issue
    :members:
.. automodule:: gitlabreq.labels
    :members:
.. automodule:: gitlabreq.milestones
    :members:
.. automodule:: gitlabreq.pipelines
    :members:
.. automodule:: gitlabreq.projects
    :members:
.. automodule:: gitlabreq.repositories
    :members:
.. automodule:: gitlabreq.session
    :members:

.. toctree::
   :maxdepth: 2
   :caption: Contents:


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
