#!/usr/bin/env python3
"""
Get gitlab file from repository
"""

import urllib.parse
from .extends import extends
from .session import Session


@extends(Session)
def get_raw_file(self, repository_id, file_path, branch="master"):
    """
    Get a raw file frome a repository

    :param repository_id: ID of the repository
    :param file_path: path of the file in the repository
    :param branch: name of the branch where getting the file in the repository
    :type repository_id: int
    :type file_path: str
    :type branch: str
    :return: Raw file
    :rtype: str
    """
    file_request = self.get(
        f"""{self.url}/projects/{str(repository_id)}/repository/files/{urllib.parse.quote(file_path, safe="")}/raw""",
        params={"ref": branch},
    )
    raw_file = file_request.content

    return raw_file
