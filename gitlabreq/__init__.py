"""
GitlabReq
"""
from .session import Session
from .file import *
from .issue import *
from .labels import *
from .milestones import *
from .pipelines import *
from .projects import *
from .repositories import *


class GitlabReq(Session):
    pass
