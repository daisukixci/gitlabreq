#!/usr/bin/env python3
"""
Getting, manipulating issues from Gitlab
"""

import json
import urllib.parse

from .extends import extends
from .session import Session


@extends(Session)
def get_issue_label_history(self, project_id, issue_id):
    """
    Get history of labels manipulations for an issue

    :param project_id: Id of the project
    :param issue_id: Id of the issue
    :type project_id: int
    :type issue_is: int
    :return: Issues list
    :rtype: list
    """
    # Make a first request to get the first page
    history_request = self.get(
        f"{self.url}/projects/{str(project_id)}/issues/{str(issue_id)}/resource_label_events",
    )
    history_list = json.loads(history_request.content)

    # Follow the next argument if present
    while "next" in history_request.links.keys():
        history_request = self.get(
            history_request.links["next"]["url"],
        )
        history_list += json.loads(history_request.content)
    return history_list


@extends(Session)
def get_all_issues(self, project=None, group=None, milestone=None, scope="all"):
    """
    Get all issues

    :return: Issues list
    :rtype: list
    """
    url = f"{self.url}/issues"
    if group:
        groups_request = self.get(f"{self.url}/groups")
        groups_list = json.loads(groups_request.content)
        for group_item in groups_list:
            if group_item["name"] == group:
                group_id = group_item["id"]
                break
        url = f"{self.url}/groups/{str(group_id)}/issues"

    if project:
        url = f"""{self.url}/projects/{urllib.parse.quote(project, safe="")}/issues"""

    params = {"scope": scope}
    if milestone:
        params["milestone"] = milestone

    # Make a first request to get the first page
    issues_request = self.get(
        url,
        params=params,
    )
    issues_list = json.loads(issues_request.content)

    # Follow the next argument if present
    while "next" in issues_request.links.keys():
        issues_request = self.get(
            issues_request.links["next"]["url"],
        )
        issues_list += json.loads(issues_request.content)
    return issues_list


@extends(Session)
def edit_milestone_issue(self, issue, group, milestone_wanted):
    """
    Change the milestone of an issue

    :param issue: Json issue from Gitlab API
    :param group: Group of the issue
    :param milestone_wanted: Milestone to put on issue
    :type issue: JSON issue
    :type group: str
    :type milestone_wanted: str
    """
    project_id = issue["project_id"]
    iid = issue["iid"]
    milestone_id = self.get_milestone_id(group, milestone_wanted)

    issue_request = self.put(
        f"{self.url}/projects/{str(project_id)}/issues/{str(iid)}",
        data={"milestone_id": milestone_id},
    )
    issue_request.raise_for_status()


@extends(Session)
def del_label(self, issue, label):
    """
    Delete a label from an issue

    :param issue: Json issue from Gitlab API
    :param label: Label to remove from issue
    """
    project_id = issue["project_id"]
    iid = issue["iid"]
    labels = list(issue["labels"])
    if label in labels:
        labels.remove(label)
    else:
        print("Label " + label + "is not present")
        return 0

    issue_request = self.put(
        f"{self.url}/projects/{str(project_id)}/issues/{str(iid)}",
        data={"labels": ",".join(labels)},
    )
    issue_request.raise_for_status()


@extends(Session)
def add_label(self, issue, label):
    """
    Add a label from an issue

    :param issue: Json issue from Gitlab API
    :param label: Label to add to issue
    """
    project_id = issue["project_id"]
    iid = issue["iid"]
    labels = set(issue["labels"])
    labels.add(label)

    issue_request = self.put(
        f"{self.url}/projects/{str(project_id)}/issues/{str(iid)}",
        data={"labels": ",".join(labels)},
    )
    issue_request.raise_for_status()


@extends(Session)
def edit_state_event_of_issue(self, issue, state):
    """
    Add a label from an issue

    :param issue: Json issue from Gitlab API
    :param state: Wanted state for the issue (close|reopen)
    :param url: Gitlab API URL
    :param token: Token to access Gitlab API
    :param ca_cert: CA cert file to access Gitlab API
    """
    project_id = issue["project_id"]
    iid = issue["iid"]

    issue_request = self.put(
        f"{self.url}/projects/{str(project_id)}/issues/{str(iid)}",
        data={"state_event": state},
    )
    issue_request.raise_for_status()


def filter_issues_by_state(issues, state):
    """
    Filter issues from list according to the state (opened/closed)

    :param issues: List of issues to filter
    :param state: State to filter on (opened/closed)
    :type issues: list
    :type state: str
    :return: Filtered issues list
    :rtype: list
    """
    issues_filtered = list()
    for issue in issues:
        if issue["state"] == state:
            issues_filtered.append(issue)
    return issues_filtered


def filter_issues_by_label(issues, *labels):
    """
    Filter issues from list according to label put on

    :param issues: List of issues to filter
    :param labels: Labels to filter the list
    :type issues: list
    :type label: str
    :return: Filtered issues list
    :rtype: list
    """
    issues_filtered = list()
    for issue in issues:
        if set(labels).issubset(issue["labels"]):
            issues_filtered.append(issue)
    return issues_filtered


def filter_issues_by_not_label(issues, *labels):
    """
    Filter issues from list according to the missing label

    :param issues: List of issues to filter
    :param labels: Labels to avoid in the list
    :type issues: list
    :type label: str
    :return: Filtered issues list
    :rtype: list
    """
    issues_filtered = list()
    for issue in issues:
        if not set(labels).issubset(issue["labels"]):
            issues_filtered.append(issue)
    return issues_filtered
