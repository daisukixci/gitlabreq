#!/usr/bin/env python3
"""
Gitlab session
"""

import requests


class Session:
    """
    Session
    """

    def __init__(self, token, url="https://gitlab.com/api/v4/", verify=True):
        """
        :param url: Gitlab API URL
        :param token: Token to access Gitlab API
        :param verify: CA cert file to access Gitlab API
        :type url: str
        :type token: str
        :type verify: str
        """

        self.url = url
        self.session = requests.Session()
        self.session.headers.update({"PRIVATE-TOKEN": token})
        self.session.verify = verify

    def get(self, url, params=None):
        """
        Get method on Gitlab API
        """
        try:
            return self.session.get(url, params=params)
        except requests.exceptions.RequestException as error:
            raise error

    def post(self, url, data=None, params=None):
        """
        Post method on Gitlab API
        """
        try:
            return self.session.post(url, data=data, params=params)
        except requests.exceptions.RequestException as error:
            raise error
