#!/usr/bin/env python3


def extends(klass):
    def decorator(func):
        setattr(klass, func.__name__, func)
        return func

    return decorator
