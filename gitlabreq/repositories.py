#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    Manipulate GitLab repositories with API
"""

import json

from .session import Session
from .extends import extends


@extends(Session)
def compare_branches(self, project_id, from_branch, to_branch):
    """
    Get compare result between two branches of a project

    :param project_id: id of the project
    :param from_branch: source branch to compare
    :param to_branch: target branch to compare
    :type project_id: int
    :type from_branch: str
    :type to_branch: str
    :return: Compare details JSON list
    :rtype: JSON
    """
    # make a first request to get the first page of gitlab projects
    request = self.get(
        f"{self.url}/projects/{str(project_id)}/repository/compare",
        params={"from": from_branch, "to": to_branch},
    )
    request.raise_for_status()
    answer = json.loads(request.content)

    # follow the next argument if present
    while "next" in request.links.keys():
        request = self.get(
            request.links["next"]["url"],
        )
        request.raise_for_status()
        answer += json.loads(request.content)
    return answer
