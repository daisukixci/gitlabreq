#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Manipulate GitLab pipelines
"""

import json
from .extends import extends
from .session import Session


@extends(Session)
def get_project_pipelines(self, project_id, follow_pages=True, **kwargs):
    """
    Get JSON list of all pipelines for a project

    :param project_id: ID of the pipeline's project
    :param follow_pages: Parse all the pages of the api return (default true)
    :param kwargs: Additional parameters to send to the API
    :type project_id: int
    :type follow_pages: boolean
    :return: Pipelines JSON list
    :rtype: JSON
    """
    # make a first request to get the first page of gitlab projects
    pipeline_request = self.get(
        f"{self.url}/projects/{str(project_id)}/pipelines",
        params=kwargs,
    )
    pipeline_request.raise_for_status()

    pipelines_list = json.loads(pipeline_request.content)

    # follow the next argument if present
    if follow_pages:
        while "next" in pipeline_request.links.keys():
            pipeline_request = self.get(
                pipeline_request.links["next"]["url"],
            )
            pipeline_request.raise_for_status()
            pipelines_list += json.loads(pipeline_request.content)

    return pipelines_list


@extends(Session)
def get_single_pipeline(self, project_id, pipeline_id):
    """
    Get JSON details of a pipeline

    :param project_id: ID of the pipeline's project
    :param pipeline_id: id of the pipeline
    :type project_id: int
    :type pipeline_id: int
    :return: Pipeline details JSON list
    :rtype: JSON
    """
    pipeline_request = self.get(
        f"{self.url}/projects/{str(project_id)}/pipelines/{str(pipeline_id)}",
    )
    pipeline_request.raise_for_status()

    return json.loads(pipeline_request.content)
