#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    Manipulate milestone on Gitlab
"""
import urllib.parse
import json
import requests

from .extends import extends
from .session import Session


@extends(Session)
def create_milestone(self, group, milestone, start_date):
    """
    Create a milestone into group

    :param group: Group which will receive the new milestone
    :param milestone: Milestone to create
    :param start_date: Begining date of milestone
    """
    milestone_request = self.post(
        f"{self.url}/groups/{urllib.parse.quote(group, safe='')}/milestones",
        data={"title": milestone, "start_date": start_date},
    )
    milestone_request.raise_for_status()


@extends(Session)
def get_milestone_id(self, group, milestone):
    """
    Get milestone ID

    :param group: Group which contains milestone
    :param milestone: Milestone to get ID
    """
    milestone_id_request = self.get(
        f"{self.url}/groups/{group}/milestones",
        params={"search": milestone},
    )
    for milestone_item in json.loads(milestone_id_request.content):
        if milestone_item["title"] == milestone:
            return milestone_item["id"]
    return 0
