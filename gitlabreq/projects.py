#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    Manipulate Gitlab projects
"""

import os
import json
import urllib.parse

from .extends import extends
from .session import Session


@extends(Session)
def get_all_projects(self, namespace=None):
    """
    Get a JSON list of all projects

    :param namespace: Namespace where get projects
    :type namespace: str
    :return: Project JSON list
    :rtype: JSON
    """
    if namespace:
        url = f"{self.url}/groups/{urllib.parse.quote(namespace, '')}/projects"
    else:
        url = f"{self.url}/projects"
    # Make a first request to get the first page of gitlab projects
    projects_request = self.get(url)
    projects_request.raise_for_status()
    projects_list = json.loads(projects_request.content)

    # Follow the next argument if present
    while "next" in projects_request.links.keys():
        projects_request = self.get(
            projects_request.links["next"]["url"],
        )
        projects_request.raise_for_status()
        projects_list += json.loads(projects_request.content)
    return projects_list


@extends(Session)
def fork_project(self, src_project, dst_namespace):
    """
    Fork a project from src to dst
    """
    encoded_src_project = urllib.parse.quote(src_project, "")
    fork_request = self.post(
        f"{self. url}/projects/{encoded_src_project}/fork",
        data={"namespace": dst_namespace},
    )
    fork_request.raise_for_status()


def clone_projects(projects_list):
    """
    Clone all projects from the projects_list

    :param projects_list: List of project to clone
    :type project: list
    """
    for project in projects_list:
        path = project["path_with_namespace"]

        if not os.path.exists(path):
            os.makedirs(path)

        os.system(
            "git clone " + project["ssh_url_to_repo"] + " " + path + "&>/dev/null"
        )


@extends(Session)
def get_project_branches(self, project_id):
    """
    Get project branches
    """

    branches_request = self.get(
        f"{self.url}/projects/{str(urllib.parse.quote(project_id, safe=''))}/repository/branches",
    )
    branches_request.raise_for_status()
    branches_list = json.loads(branches_request.content)

    # follow the next argument if present
    while "next" in branches_request.links.keys():
        branches_request = self.get(
            branches_request.links["next"]["url"],
        )
        branches_request.raise_for_status()
        branches_list += json.loads(branches_request.content)
    return branches_list


@extends(Session)
def get_project_members(self, project_id):
    """
    Get members of a project

    :param project_id: id of the project
    :type project_id: int
    :return: Member JSON list
    :rtype: JSON
    """

    members_request = self.get(
        f"{self.url}/projects/{str(urllib.parse.quote(project_id, safe=''))}/members/all",
    )
    members_request.raise_for_status()
    members_list = json.loads(members_request.content)

    # follow the next argument if present
    while "next" in members_request.links.keys():
        members_request = self.get(
            members_request.links["next"]["url"],
        )
        members_request.raise_for_status()
        members_list += json.loads(members_request.content)
    return members_list
