#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Manage Gitlab labels
"""

import json

from .extends import extends
from .session import Session


@extends(Session)
def remove_labels_from_project_list(self, project_lists):
    """
    Remove labels from projects list
    """
    # For each project get the list label and for each label
    # push a delete request
    for project in project_lists:
        labels_request = self.get(
            f"{self.url}/projects/{str(project['id'])}/labels",
        )
        labels_request.raise_for_status()

        labels_list = json.loads(labels_request.content)
        for label in labels_list:
            delete_request = self.delete(
                f"{self.url}/projects/{str(project['id'])}/labels",
                params={"name": label["name"]},
            )
            delete_request.raise_for_status()
