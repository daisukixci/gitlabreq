import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="gitlabreq-daisukixci",
    version="0.0.1",
    author="Daisukixci",
    author_email="daisuki@tuxtrooper.com",
    description="A Gitlab API wrapper",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/daisukixci/gitlabreq",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires=">=3.6",
    install_requires=[
        "requests",
    ],
)
